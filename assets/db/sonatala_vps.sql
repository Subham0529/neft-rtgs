-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 23, 2021 at 11:51 AM
-- Server version: 10.2.39-MariaDB
-- PHP Version: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sonatala_vps`
--

-- --------------------------------------------------------

--
-- Table structure for table `tt_dbtl`
--

CREATE TABLE `tt_dbtl` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(500) NOT NULL,
  `cbs_account` varchar(30) NOT NULL,
  `transaction_amount` float(10,2) NOT NULL,
  `transaction_date` date NOT NULL,
  `pacs_name` varchar(500) NOT NULL,
  `system_date` date NOT NULL,
  `narration` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tt_neft`
--

CREATE TABLE `tt_neft` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(500) NOT NULL,
  `cbs_account` varchar(50) NOT NULL,
  `ref_no` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `transaction_amount` float(20,2) NOT NULL,
  `transaction_date` date NOT NULL,
  `rem_details` varchar(200) NOT NULL,
  `pacs_name` varchar(200) NOT NULL,
  `system_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tt_out_post`
--

CREATE TABLE `tt_out_post` (
  `id` int(11) NOT NULL,
  `tnx_id` varchar(50) NOT NULL,
  `source_account_number` varchar(200) NOT NULL,
  `destination_account_number` varchar(200) NOT NULL,
  `ifsc_code` varchar(200) NOT NULL,
  `txn_amt` float(20,2) NOT NULL,
  `pacs_id` varchar(30) NOT NULL,
  `ben_name` varchar(200) NOT NULL,
  `ben_add` varchar(500) NOT NULL,
  `commission` float(20,2) NOT NULL,
  `api_key` varchar(200) NOT NULL,
  `error_count` varchar(50) DEFAULT NULL,
  `error_msg` text DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tt_out_queue`
--

CREATE TABLE `tt_out_queue` (
  `id` int(11) NOT NULL,
  `transaction_no` varchar(50) NOT NULL,
  `pacs_id` varchar(50) NOT NULL,
  `cbs_queue_no` varchar(50) NOT NULL,
  `cbs_queue_no2` varchar(50) NOT NULL,
  `cbs_queue_no_status` varchar(50) NOT NULL,
  `cbs_queue_no2_status` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tt_out_res`
--

CREATE TABLE `tt_out_res` (
  `id` int(11) NOT NULL,
  `tnx_id` varchar(50) NOT NULL,
  `transaction_no` varchar(30) NOT NULL,
  `transaction_date` date NOT NULL,
  `cbs_account` varchar(30) NOT NULL,
  `receiver_account` varchar(30) NOT NULL,
  `receiver_ifsc` varchar(30) NOT NULL,
  `receiver_name` varchar(150) NOT NULL,
  `transaction_amount` float(20,2) NOT NULL,
  `utr` varchar(30) DEFAULT NULL,
  `pacs_name` varchar(150) NOT NULL,
  `pacs_id` varchar(20) NOT NULL,
  `status` varchar(30) NOT NULL,
  `system_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tt_return_neft`
--

CREATE TABLE `tt_return_neft` (
  `id` int(11) NOT NULL,
  `cbs_account` varchar(30) NOT NULL,
  `ref_no` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `transaction_amount` float(20,2) NOT NULL,
  `transaction_date` date NOT NULL,
  `rem_details` varchar(50) NOT NULL,
  `pacs_name` varchar(500) NOT NULL,
  `system_date` date NOT NULL,
  `ejection_reason` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tt_rtgs`
--

CREATE TABLE `tt_rtgs` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(500) NOT NULL,
  `cbs_account` varchar(30) NOT NULL,
  `ref_no` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `transaction_amount` float(20,2) NOT NULL,
  `transaction_date` date NOT NULL,
  `rem_details` varchar(500) NOT NULL,
  `pacs_name` varchar(500) NOT NULL,
  `system_date` date NOT NULL,
  `guardian_name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tt_dbtl`
--
ALTER TABLE `tt_dbtl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tt_neft`
--
ALTER TABLE `tt_neft`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tt_out_post`
--
ALTER TABLE `tt_out_post`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tnx_id` (`tnx_id`);

--
-- Indexes for table `tt_out_queue`
--
ALTER TABLE `tt_out_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tt_out_res`
--
ALTER TABLE `tt_out_res`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tt_return_neft`
--
ALTER TABLE `tt_return_neft`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tt_rtgs`
--
ALTER TABLE `tt_rtgs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tt_dbtl`
--
ALTER TABLE `tt_dbtl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_neft`
--
ALTER TABLE `tt_neft`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_out_post`
--
ALTER TABLE `tt_out_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_out_queue`
--
ALTER TABLE `tt_out_queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_out_res`
--
ALTER TABLE `tt_out_res`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_return_neft`
--
ALTER TABLE `tt_return_neft`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tt_rtgs`
--
ALTER TABLE `tt_rtgs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
