<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

	public function __construct() {

        // $this->sysdate  = $_SESSION['sys_date'];

        parent::__construct();

        //For Individual Functions
      	$this->load->dbutil();
    }
  
  	function get_inward_neft(){
      // START API CALL //
      $curl = curl_init();

      curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://www.ipksapiwbscb.net/WBSCBAPI/resources/INWARD/NEFT',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS =>'{
            "pacs_id": "'.PACSID.'",
            "api_key": "'.APIKEY.'",
            "date": "'.date('d-m-Y').'"
        }',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        //echo $response;
      	// END API CALL //
      	
      	// INSERT DATA INTO DATABASE //
      	$input = array();
      	if(strlen($response) > 32){
          $res = json_decode($response, true);
          //$dt = $res[0];
          foreach($res as $dt){
            foreach($dt as $k => $v){
              if(rtrim($k) == 'TRANSACTION DATE' || rtrim($k) == 'SYSTEM DATE'){
                $input[strtolower(str_replace(' ', '_', rtrim($k)))] = date('Y-m-d', strtotime($v));
              }else{
                $input[strtolower(str_replace(' ', '_', rtrim($k)))] = $v;
              }
            }
            $this->db->where(array(
              'ref_no' => $input['ref_no']
            ));
            $query = $this->db->get('tt_neft');
            if($query->num_rows() > 0){

            }else{
              $this->db->insert('tt_neft', $input);
            }
            
          }
        }
    }
  
  	function corn_outneft_post(){
      $res = '';
      $this->db->where(array(
      	'DATE(created_at)' => date('Y-m-d')
      ));
      $query = $this->db->get('tt_out_post');
      //echo $this->db->last_query();var_dump($query->num_rows());exit;
      if($query->num_rows() > 0){
        $res = $query->result();
        foreach($res as $k => $val){
          if($val->error_count == '' || $val->error_count == null){
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => 'https://www.ipksapiwbscb.net/WBSCBAPI/resources/OUTNEFT/POST',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'POST',
              CURLOPT_POSTFIELDS =>'{
                "source_account_number": "'.$val->source_account_number.'",
                "destination_account_number": "'.$val->destination_account_number.'",
                "ifsc_code": "'.$val->ifsc_code.'",
                "txn_amt": "'.$val->txn_amt.'",
                "pacs_id": "'.$val->pacs_id.'",
                "ben_name": "'.$val->ben_name.'",
                "ben_add": "'.$val->ben_add.'",
                "commission": "'.$val->commission.'",
                "api_key": "'.$val->api_key.'"
              }',
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
              ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            echo $response;
            $res_data = json_decode($response, true);
            $input_1 = array(
              'error_count' => $res_data['Error Count '],
              'error_msg' => $res_data['Error Msg ']
            );
            //var_dump($res_data['Error Count']);exit;
            $this->db->where(array('tnx_id' => $val->tnx_id));
            $query_get = $this->db->get('tt_out_post');
            if($query_get->num_rows() > 0){
              $row_dt = $query_get->row();
              if($row_dt->error_count != '' || $row_dt->error_count != null){
                echo 'Subham';
              }else{
                $this->db->where(array('tnx_id' => $val->tnx_id));
                $this->db->update('tt_out_post', $input_1);  
              }

            }
            if($res_data['Error Count '] == 0){
              $this->cron_outneft_get($val->tnx_id);
            }
          }
          
          //var_dump();
        }
      }
    }
  
    function cron_outneft_get($tnx_id){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://www.ipksapiwbscb.net/WBSCBAPI/resources/OUTNEFT/GET',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
              "pacs_id": "'.PACSID.'",
              "api_key": "'.APIKEY.'",
              "date": "'.date("d-m-Y").'"
          }',
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json'
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);
      echo $response;
      
      $res = json_decode($response, true);
      //var_dump($res);exit;
      $input = array();
      if($res){
        foreach($res as $dt){
          foreach($dt as $k => $v){
            if(rtrim($k) == 'TRANSACTION DATE' || rtrim($k) == 'SYSTEM DATE'){
              $input[strtolower(str_replace(' ', '_', rtrim($k)))] = date('Y-m-d', strtotime($v));
            }else{
              $input[strtolower(str_replace(' ', '_', rtrim($k)))] = $v;
            }
          }
          $input_1 = array('utr' => $input['utr']);
          $tnx_id > 0 ? $input['tnx_id'] = $tnx_id : '';
          $this->db->where(array(
            'transaction_no' => $input['transaction_no']
          ));
          $query = $this->db->get('tt_out_res');
          if($query->num_rows() > 0){
            $this->db->where(array(
            	'transaction_no' => $input['transaction_no']
          	));
            $this->db->update('tt_out_res', $input_1);
          }else{
              $this->db->insert('tt_out_res', $input);
          }
        }
      }
      
    }
  
  	function cron_inward_rtgs(){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://www.ipksapiwbscb.net/WBSCBAPI/resources/INWARD/RTGS',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
              "pacs_id": "'.PACSID.'",
              "api_key": "'.APIKEY.'",
              "date": "'.date("d-m-Y").'"
          }',
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json'
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);
      echo $response;
      
      $res = json_decode($response, true);
      if($res){
        foreach($res as $dt){
          foreach($dt as $k => $v){
            if(rtrim($k) == 'TRANSACTION DATE' || rtrim($k) == 'SYSTEM DATE'){
              $input[strtolower(str_replace(' ', '_', rtrim($k)))] = date('Y-m-d', strtotime($v));
            }else{
              $input[strtolower(str_replace(' ', '_', rtrim($k)))] = $v;
            }
          }
          $this->db->where(array('ref_no' => $input['ref_no']));
          $query = $this->db->get('tt_rtgs');
          if($query->num_rows() > 0){
            
          }else{
            $this->db->insert('tt_rtgs', $input);
          }
        }
      }
    }
  	
  	function cron_return_neft(){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://www.ipksapiwbscb.net/WBSCBAPI/resources/INWARD/RETURN_NEFT',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
              "pacs_id": "'.PACSID.'",
              "api_key": "'.APIKEY.'",
              "date": "'.date("d-m-Y").'"
          }',
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json'
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);
      echo $response;
      
      $res = json_decode($response, true);
      //var_dump($res);exit;
      $input = array();
      if($res){
        foreach($res as $dt){
          foreach($dt as $k => $v){
            if(rtrim($k) == 'TRANSACTION DATE' || rtrim($k) == 'SYSTEM DATE'){
              $input[strtolower(str_replace(' ', '_', rtrim($k)))] = date('Y-m-d', strtotime($v));
            }else{
              $input[strtolower(str_replace(' ', '_', rtrim($k)))] = $v;
            }
          }
          $this->db->where(array('ref_no' => $input['ref_no']));
          $query = $this->db->get('tt_return_neft');
          if($query->num_rows() > 0){
            
          }else{
            $this->db->insert('tt_return_neft', $input);
          }
        }
      }
    }
  
  	function cron_dbtl_get(){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://www.ipksapiwbscb.net/WBSCBAPI/resources/DBTL/GET',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
              "pacs_id": "'.PACSID.'",
              "api_key": "'.APIKEY.'",
              "date": "'.date("d-m-Y").'"
          }',
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json'
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);
      echo $response;
      
      $res = json_decode($response, true);
      //var_dump($res);exit;
      $input = array();
      if($res){
        foreach($res as $dt){
          foreach($dt as $k => $v){
            if(str_replace(':', '', rtrim($k)) == 'TRANSACTION DATE' || str_replace(':', '', rtrim($k)) == 'SYSTEM DATE'){
              $input[strtolower(str_replace(':', '', str_replace(' ', '_', rtrim($k))))] = date('Y-m-d', strtotime($v));
            }else{
              $input[strtolower(str_replace(':', '', str_replace(' ', '_', rtrim($k))))] = $v;
            }
          }
          $this->db->where($input);
          $query = $this->db->get('tt_dbtl');
          if($query->num_rows() > 0){
              
          }else{
              $this->db->insert('tt_dbtl', $input);
          }
        }
      }
      
    }
  
  	function cron_outneft_queue(){
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://www.ipksapiwbscb.net/WBSCBAPI/resources/OUTNEFT/QUEUE',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
              "pacs_id": "'.PACSID.'",
              "api_key": "'.APIKEY.'",
              "date": "'.date("d-m-Y").'"
          }',
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json'
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);
      echo $response;
      
      $res = json_decode($response, true);
      //var_dump($res);exit;
      $input = array();
      if($res){
        foreach($res as $dt){
          foreach($dt as $k => $v){
            $input[strtolower(str_replace(':', '', str_replace(' ', '_', rtrim($k))))] = $v;
          }
          $this->db->where(array('transaction_no' => $input['transaction_no']));
          $query = $this->db->get('tt_out_queue');
          if($query->num_rows() > 0){
              
          }else{
              $this->db->insert('tt_out_queue', $input);
          }
        }
      }
      
    }
  
  	function test(){
      //echo '<pre>';
      $response = '[{"CUSTOMER NAME ":"xxxx","CBS ACCOUNT ":"xxx","REF NO ":"xxx","STATUS ":"POSTED","TRANSACTION AMOUNT ":"250000","TRANSACTION DATE ":"08-NOV-2019","REM DETAILS ":"NA","PACS NAME ":"xxxx","SYSTEM DATE ":"19-12-2019","GURDIAN NAME ":"xxxx"}]';
      $res_data = json_decode($response, true);
      $res = json_decode($response, true);
      //var_dump($res);exit;
      $input = array();
      if($res){
        foreach($res as $dt){
          foreach($dt as $k => $v){
            if(rtrim($k) == 'TRANSACTION DATE' || rtrim($k) == 'SYSTEM DATE'){
              $input[strtolower(str_replace(' ', '_', rtrim($k)))] = date('Y-m-d', strtotime($v));
            }else{
              $input[strtolower(str_replace(' ', '_', rtrim($k)))] = $v;
            }
          }
          $this->db->where(array('ref_no' => $input['ref_no']));
          $query = $this->db->get('tt_rtgs');
          if($query->num_rows() > 0){
            
          }else{
            $this->db->insert('tt_rtgs', $input);
          }
        }
      }
      //$this->db->update('tt_out_post', $res_data);
      
      
      var_dump($input);
    }
}
?>