<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct() {

        // $this->sysdate  = $_SESSION['sys_date'];

        parent::__construct();

        //FOR IMPORT SQL DATA TO CSV FORMAT
      	$this->load->dbutil();
    }
  	
  	// INWARD NEFT FROM tt_neft TABLE //
	function inward_neft()
	{
      	$trans_dt = $this->input->get('trans_dt');
      
      	$this->db->where(array('transaction_date' => $trans_dt));
		$query = $this->db->get('tt_neft');
      
        echo json_encode($query->result());
	}
  	
  	// INWARD RTGS FROM tt_rtgs TABLE //
    function inward_rtgs()
    {
      $trans_dt = $this->input->get('trans_dt');
      
      $this->db->where(array('transaction_date' => $trans_dt));
      $query = $this->db->get('tt_rtgs');
      
      echo json_encode($query->result());
    }
  
  	// INWARD DBTL FROM tt_dbtl TABLE //
  	function inward_dbtl()
    {
      $trans_dt = $this->input->get('trans_dt');
      
      $this->db->where(array('transaction_date' => $trans_dt));
      $query = $this->db->get('tt_dbtl');
      
      echo json_encode($query->result());
    }
  
  	// INSERT OUTNEFT POST DATA FROM USER IN tt_out_post TABLE // 
  	function outneft_post(){
      $data = $this->input->get();
      if($data['tnx_id']!= '' && $data['source_account_number']!= '' && $data['ifsc_code']!= '' && $data['txn_amt']!= '' && $data['pacs_id']!= '' && $data['ben_name']!= '' && $data['ben_add']!= '' && $data['commission']!= '' && $data['api_key']!= ''){
        $this->db->where(array('tnx_id' => $data['tnx_id']));
        $query = $this->db->get('tt_out_post');
        if($query->num_rows() == 0){
          if($this->db->insert('tt_out_post', $data)){
            $data['success'] = 'Data Inserted Successfully';
          }else{
            $data['success'] = 'Data Not Inserted';
          }  
        }else{
        	$data['success'] = 'Data Not Inserted';
          	$data['error'] = 'Dublicate Fields Occurs';  
        }
      	  
      }else{
        $data['success'] = 'Data Not Inserted';
        $data['error'] = 'Empty Fields Occurs';
      }
      
      echo json_encode($data);
    }
  	
  	// OUTNEFT GET FROM tt_out_res TABLE //
  	function outneft_get()
    {
      $trans_dt = $this->input->get('trans_dt');
      
      $this->db->where(array('transaction_date' => $trans_dt));
      $query = $this->db->get('tt_out_res');
      
      echo json_encode($query->result());
    }
  
  	// INWARD RETURN NEFT FROM tt_return_neft TABLE //
  	function inward_return_neft()
    {
      $trans_dt = $this->input->get('trans_dt');
      
      $this->db->where(array('transaction_date' => $trans_dt));
      $query = $this->db->get('tt_return_neft');
      //echo $this->db->last_query();exit;
      
      echo json_encode($query->result());
    }
  
  	// OUTNEFT QUEUE FROM tt_out_queue TABLE //
  	function outneft_queue()
    {
      $trans_dt = $this->input->get('trans_dt');
      
      $this->db->where(array('DATE(created_at)' => $trans_dt));
      $query = $this->db->get('tt_out_queue');
      //echo $this->db->last_query();exit;
      
      echo json_encode($query->result());
    }
  	
  	// OUTNEFT POST RESPONSE FROM tt_out_post TABLE //
  	function outneft_post_res()
    {
      $trans_dt = $this->input->get('trans_dt');
      
      $this->db->select('tnx_id, error_count, error_msg');
      $this->db->where(array('DATE(created_at)' => $trans_dt));
      $query = $this->db->get('tt_out_post');
      //echo $this->db->last_query();exit;
      
      echo json_encode($query->result());
    }
  
}
